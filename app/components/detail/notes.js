 
import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"
import Icon from 'react-native-vector-icons/Ionicons';

const Image = (props) => {

    console.warn(props.route.params.data.image,"propsdata")

    return (
      <View style={styles.container}>
        <Text style={styles.addressStyle}>
                <Icon name="lock-closed" size={12} color={Var.COLOR_BLACK} /> Catatan Internal
            </Text>
            <Text style={styles.captionStyle}>
                Hanya akan dilihat oleh Marketing satu perusahaan.
            </Text>
            <Text style={styles.alamatStyle}>
            {"\n"}Alamat Details: {"\n"}
                NavaPark Cluster Moonlight Blok 38D No.55 
            </Text>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 20,
    marginRight: 20
  },
  addressStyle: {
    fontFamily: Var.FONT_BOLD,
    color: Var.COLOR_BLACK
  },
  captionStyle: {
    color: Var.COLOR_BROWN,
    fontSize: 10
  },
  alamatStyle: {
    color: Var.COLOR_BLACK
  },
  labelKomisiStyle: {
    backgroundColor: Var.COLOR_BROWN,
    padding: 10,
    borderRadius: 10,
    width: 45,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },
  textKomisiTitleStyle: {
    color: Var.COLOR_WHITE,
    fontSize: 8
  },
  textKomisiStyle: {
    color: Var.COLOR_WHITE,
    fontFamily: Var.FONT_BOLD,
    fontSize: 16
  },
  textArsipTitleStyle: {
    fontFamily: Var.FONT_BOLD,
    color: Var.COLOR_BROWN
  },
  labelArsipStyle: {
    borderStyle: 'dotted',
    borderColor: Var.COLOR_BROWN,
    borderRadius: 1,
    borderWidth: 1,
    transform: ([{ rotateZ: '-10deg' }]),
    backgroundColor: "#FFFFFFE6",
    padding: 10
  }
});

export default Image;