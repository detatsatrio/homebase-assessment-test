 
import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"

const Image = (props) => {

    return (
      <View style={styles.container}>
        <ImageBackground 
          source={props.data.image}
          style={{
            width: "100%",
            height: 131,
          }}
        > 
          
          {props.data.komisi != undefined && (
            <View style={{
              flex: 1,
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
            }}>
              <View style={styles.labelKomisiStyle}>
                <Text style={styles.textKomisiTitleStyle}>
                  Komisi
                </Text>
                <Text style={styles.textKomisiStyle}>
                  {props.data.komisi}
                </Text>
              </View>
            </View>
          )}

          {props.data.isRent != undefined && (
            <View style={{
              position: 'absolute',
              top: 0, left: 0, right: 0, bottom: 0, 
              justifyContent: 'center',
              alignItems: 'center',
            }}>
              <View style={styles.labelArsipStyle}>
                <Text style={styles.textArsipTitleStyle}>
                  T E R S E W A
                </Text>
              </View>
            </View>
          )}

          {props.data.isSold != undefined && (
            <View style={{
              position: 'absolute',
              top: 0, left: 0, right: 0, bottom: 0, 
              justifyContent: 'center',
              alignItems: 'center',
            }}>
              <View style={styles.labelArsipStyle}>
                <Text style={styles.textArsipTitleStyle}>
                  T E R J U A L
                </Text>
              </View>
            </View>
          )}
          </ImageBackground>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  labelKomisiStyle: {
    backgroundColor: Var.COLOR_BROWN,
    padding: 10,
    borderRadius: 10,
    width: 45,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },
  textKomisiTitleStyle: {
    color: Var.COLOR_WHITE,
    fontSize: 8
  },
  textKomisiStyle: {
    color: Var.COLOR_WHITE,
    fontFamily: Var.FONT_BOLD,
    fontSize: 16
  },
  textArsipTitleStyle: {
    fontFamily: Var.FONT_BOLD,
    color: Var.COLOR_BROWN
  },
  labelArsipStyle: {
    borderStyle: 'dotted',
    borderColor: Var.COLOR_BROWN,
    borderRadius: 1,
    borderWidth: 1,
    transform: ([{ rotateZ: '-10deg' }]),
    backgroundColor: "#FFFFFFE6",
    padding: 10
  }
});

export default Image;