 
import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"

import Icon from 'react-native-vector-icons/Ionicons';

const Header = (props) => {
    return (
      <View style={styles.container}>
        <View style={styles.leftPanel}>
          <View>
            <ImageBackground
                style={styles.profilePhotoStyle}
                source={props.data.thumbnail}
            >
                <View style={styles.borderStyle}>
                </View>
            </ImageBackground>
          </View>

          <View style={styles.nameContainerStyle}>
            <Text
                style={styles.nameStyle}
            >
                {props.data.name}
            </Text>
            <Text
                style={styles.companyStyle}
            >
                {props.data.company}
            </Text>
          </View>
        </View>
        <View style={styles.rightPanel}>
            <Icon name="ellipsis-vertical" size={20} color={Var.COLOR_BLACK} />
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  profilePhotoStyle: {
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center'
  },
  nameStyle: {
    fontFamily: Var.FONT_BOLD,
    fontSize: 14,
    color: Var.COLOR_BLACK
  },
  nameContainerStyle: {
    marginLeft: 15,
    justifyContent: 'center',
  },
  companyStyle: {
    fontFamily: Var.FONT_REGULER,
    fontSize: 12,
    color: Var.COLOR_BLACK
  },
  leftPanel: {
    flexDirection: 'row'
  },
  rightPanel: {
    justifyContent: 'center'
  }
});

export default Header;