import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"

import Header from "./header"
import Image from "./image"
import Desc from "./desc"
import Info from "./info"

const Card = (props) => {
    const [data, setData] = useState(props.data)

    console.warn(props," propsprops")

    return (
      <TouchableOpacity 
        onPress={()=>props.navigation.navigate('Detail',{
            data: data
            }
          )}
        style={styles.container}>
        <Header 
            data={data}
        />
        <Image 
            data={data}
        />
        <Desc 
           data={data}
        />
        <Info 
           data={data}
        />
      </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Var.COLOR_WHITE,
    borderRadius: 15,
    marginBottom: 15,
    
  }
});

export default Card;