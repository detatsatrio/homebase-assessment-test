import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, TouchableOpacity, FlatList} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"

import Listing from "HomeBaseTest/app/components/menu/listing"
import Favorite from "HomeBaseTest/app/components/menu/favorite"
import Arsip from "HomeBaseTest/app/components/menu/arsip"

import { listingData } from "HomeBaseTest/app/store/dummy"

import Card from "HomeBaseTest/app/components/card"

const Menu = (props) => {
    const [index, setIndex] = useState(0)

    const title = (name, current) =>{
        return(
            <TouchableOpacity 
                onPress={()=>setIndex(current)}
                style={[styles.titleStyle, current == index ? styles.titleStyleActive : styles.titleStyleNonActive]}>
                <Text style={[styles.textTitleStyle, current == index ? styles.textTitleActiveStyle : styles.textTitleNonActiveStyle]}>
                    {name}
                </Text>
            </TouchableOpacity>
        )
    }

    const renderItem = ({ item }) => (
        <Text>
            {item.name}
        </Text>
      );

    return (
      <View 
        {...props}
        style={styles.container}>
        <View>
            <View style={styles.menuStyle}>
                {title("Listing", 0)}
                {title("Favorite", 1)}
                {title("Arsip", 2)}
            </View>

            {index == 0 && (
                <Listing 
                  {...props}
                />
            )}
            {index == 1 && (
                <Favorite />
            )}
            {index == 2 && (
                <Arsip />
            )}
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: Var.COLOR_WHITE
    flex: 1, 
  },
  menuStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleStyle: {
    width: "33.3%",
    alignItems: 'center',
    paddingBottom: 10,
  },
  textTitleStyle: {
    fontFamily: Var.FONT_DEMI,
    fontSize: 14
  },
  textTitleActiveStyle:{
    color: Var.COLOR_BROWN
  },
  textTitleNonActiveStyle: {
    color: Var.COLOR_BLACK
  },
  contentStyle: {
    flex: 1
  },
  titleStyleActive: {
    borderBottomColor: Var.COLOR_BROWN,
    borderBottomWidth: 2
  }
});

export default Menu;