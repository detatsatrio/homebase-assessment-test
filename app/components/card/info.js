 
import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"

const Info = (props) => {
    return (
      <View style={styles.container}>
        <View style={styles.divider} />
        <View style={styles.wrapper}>
          <View style={styles.contentDescription}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <ImageBackground 
                source={require("HomeBaseTest/app/assets/room.png")}
                style={styles.imageDescriptionStyle}
                resizeMode="contain"
              />
              <Text style={styles.fontDataStyle}>
                {props.data.room}
              </Text>
            </View>
            <Text style={styles.fontStyle}>
              K. Tidur
            </Text>
          </View>
          <View style={[styles.contentDescription, styles.leftBorderStyle]}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <ImageBackground 
                source={require("HomeBaseTest/app/assets/bath.png")}
                style={styles.imageDescriptionStyle}
                resizeMode="contain"
              />
              <Text style={styles.fontDataStyle}>
                {props.data.bath}
              </Text>
            </View>
            <Text style={styles.fontStyle}>
              K. Mandi
            </Text>
          </View>
          <View style={[styles.contentDescription, styles.leftBorderStyle]}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <ImageBackground 
                source={require("HomeBaseTest/app/assets/square.png")}
                style={styles.imageDescriptionStyle}
                resizeMode="contain"
              />
              <Text style={styles.fontDataStyle}>
                {props.data.square}
              </Text>
            </View>
            <Text style={styles.fontStyle}>
              L. Tanah
            </Text>
          </View>
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  divider: {
    borderBottomWidth: 1,
    borderBottomColor: "#ececec",
    marginLeft: 15,
    marginRight: 15,
    marginTop: -10
  },
  imageDescriptionStyle: {
    width: 25,
    height: 25,
    marginRight: 10
  },
  contentDescription: {
    padding: 15
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  fontStyle: {
    fontSize: 10,
    fontFamily: Var.FONT_MEDIUm
  },
  fontDataStyle: {
    fontFamily: Var.FONT_BOLD,
    fontSize: 16,
    color: Var.COLOR_BLACK
  },
  leftBorderStyle: {
    borderLeftColor: "#ececec", 
    borderLeftWidth: 1,
    paddingTop: 15,
    paddingBottom: 15
  }
});

export default Info;