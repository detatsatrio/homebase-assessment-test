 
import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"

const Image = (props) => {

    console.warn(props.route.params.data.image,"propsdata")

    return (
      <View style={styles.container}>
        <ImageBackground 
          source={props.route.params.data.image}
          style={{
            width: "100%",
            height: 200,
          }}
        > 
          </ImageBackground>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  labelKomisiStyle: {
    backgroundColor: Var.COLOR_BROWN,
    padding: 10,
    borderRadius: 10,
    width: 45,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },
  textKomisiTitleStyle: {
    color: Var.COLOR_WHITE,
    fontSize: 8
  },
  textKomisiStyle: {
    color: Var.COLOR_WHITE,
    fontFamily: Var.FONT_BOLD,
    fontSize: 16
  },
  textArsipTitleStyle: {
    fontFamily: Var.FONT_BOLD,
    color: Var.COLOR_BROWN
  },
  labelArsipStyle: {
    borderStyle: 'dotted',
    borderColor: Var.COLOR_BROWN,
    borderRadius: 1,
    borderWidth: 1,
    transform: ([{ rotateZ: '-10deg' }]),
    backgroundColor: "#FFFFFFE6",
    padding: 10
  }
});

export default Image;