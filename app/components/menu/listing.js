import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, TouchableOpacity, TouchableWithoutFeedback, FlatList, SafeAreaView} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"

import { listingData } from "HomeBaseTest/app/store/dummy"

import Card from "HomeBaseTest/app/components/card"

const Listing = (props) => {

    return (
      <FlatList
        data={listingData}
        keyExtractor={item => item.id}
        showsVerticalScrollIndicator={false}
        renderItem={({ item, index }) => (
            <Card {...props} data={item} index={index} />
        )}
        style={styles.flatlist}
      />
    );
}

const styles = StyleSheet.create({
  flatlist: {
    backgroundColor: '#ececec',
    padding: 30,
  }
 
});

export default Listing;