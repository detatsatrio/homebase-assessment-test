import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, TouchableOpacity, FlatList} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"

import { arsipData } from "HomeBaseTest/app/store/dummy"

import Card from "HomeBaseTest/app/components/card"

const Arsip = () => {

    return (
      <FlatList
        data={arsipData}
        keyExtractor={item => item.id}
        showsVerticalScrollIndicator={false}
        renderItem={({ item, index }) => (
            <Card data={item} index={index} />
        )}
        style={styles.flatlist}
      />
    );
}

const styles = StyleSheet.create({
  flatlist: {
    backgroundColor: '#ececec',
    padding: 30,
  }
 
});

export default Arsip;