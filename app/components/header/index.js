import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, TouchableOpacity} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"

import Icon from 'react-native-vector-icons/Ionicons';

const Header = () => {
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
            <View style={styles.backHeaderStyle}>
                <Icon name="arrow-back" size={30} color={Var.COLOR_BLACK} />
            </View>
            <View style={styles.mainHeaderStyle}>
            <ImageBackground
                style={styles.profilePhotoStyle}
                source={require('HomeBaseTest/app/assets/dummy/profile.png')}
            >
                <View style={styles.borderStyle}>
                </View>
            </ImageBackground>

            <Text
                style={styles.nameStyle}
            >
                Henry Scott
            </Text>
            
            <Text
                style={styles.captionStyle}
            >
                Member Broker Century 21 BSD City 
            </Text>
          </View>
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Var.COLOR_WHITE,
    padding: 20
  },
  wrapper:{
    flexDirection: 'row',
    width: "100%",
  },
  profilePhotoStyle: {
    width: 65,
    height: 65,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  nameStyle: {
    fontFamily: Var.FONT_BOLD,
    fontSize: 14,
    color: Var.COLOR_BLACK
  },
  captionStyle: {
    fontFamily: Var.FONT_REGULAR,
    color: Var.COLOR_BLACK
  },
  borderStyle: {
    borderWidth: 1,
    borderRadius: 75/2,
    width: 75,
    height: 75,
    borderColor: Var.COLOR_BROWN
  },
  mainHeaderStyle: {
    justifyContent: 'center', 
    alignItems: 'center',
    width: "80%"
  },
  backHeaderStyle: {
    justifyContent: "flex-start",
    alignItems: "flex-start",
  }
});

export default Header;

