export const listingData = [
    {
        id: "#80889",
        thumbnail: require("HomeBaseTest/app/assets/dummy/profile.png"),
        name: "Henry Scott",
        company: "Century 21 BSD City",
        image: require("HomeBaseTest/app/assets/dummy/listing-one.png"),
        title: "Nava Park BSD City",
        price: "Rp 6.500.000.000",
        type: "Rumah",
        label: 0, // 0: Dijual, 1: Disewa
        address: "Jl. Edutown Kav III.1, BSD, Tangerang Selatan",
        room: "3+1",
        bath: "3+1",
        square: "300m"
    },
    {
        id: "#80888",
        thumbnail: require("HomeBaseTest/app/assets/dummy/profile.png"),
        name: "Henry Scott",
        company: "Century 21 BSD City",
        image: require("HomeBaseTest/app/assets/dummy/listing-two.png"),
        title: "Woody Residence Foresta",
        title_type: 1, // 1: private
        price: "Rp 10.000.000 / Bulan",
        type: "Apartemen",
        label: 1, // 0: Dijual, 1: Disewa
        address: "Jl. Edutown Kav III.1, BSD, Tangerang Selatan",
        room: "4",
        bath: "4",
        square: "275m"
    }
]

export const favoriteData = [
    {
        id: "#80887",
        thumbnail: require("HomeBaseTest/app/assets/dummy/profile-2.png"),
        name: "Andrea Collins",
        company: "Century 21 BSD City",
        image: require("HomeBaseTest/app/assets/dummy/favorite-1.png"),
        komisi: "2%",
        title: "B Residence BSD Signature Lotus",
        price: "Rp 2.500.000.000",
        type: "Rumah",
        label: 0, // 0: Dijual, 1: Disewa
        address: "Jl. Edutown Kav III.1, BSD, Tangerang Selatan",
        room: "3+1",
        bath: "2+1",
        square: "118m"
    },
    {
        id: "#80886",
        thumbnail: require("HomeBaseTest/app/assets/dummy/profile-3.png"),
        name: "Stewart Vale",
        company: "Century 21 Breeze",
        image: require("HomeBaseTest/app/assets/dummy/favorite-2.png"),
        komisi: "2%",
        title: "The Branz Apartment BSD City",
        title_type: 1, // 1: private
        price: "Rp 2.250.000.000",
        type: "Rumah",
        label: 0, // 0: Dijual, 1: Disewa
        address: "Jl. Edutown Kav III.1, BSD, Tangerang Selatan",
        room: "3+1",
        bath: "2+1",
        square: "85m"
    }
]

export const arsipData = [
    {
        id: "#80885",
        thumbnail: require("HomeBaseTest/app/assets/dummy/profile.png"),
        name: "Henry Scott",
        company: "Century 21 BSD City",
        image: require("HomeBaseTest/app/assets/dummy/arsip-1.png"),
        komisi: "2%",
        title: "B Residence BSD Signature Lotus",
        price: "Rp 4.000.000 / Bulan",
        type: "Apartemen",
        label: 0, // 0: Dijual, 1: Disewa
        address: "Jl. Edutown Kav III.1, BSD, Tangerang Selatan",
        room: "2",
        bath: "2",
        square: "50m",
        isRent: true
    },
    {
        id: "#80884",
        thumbnail: require("HomeBaseTest/app/assets/dummy/profile.png"),
        name: "Henry Scott",
        company: "Century 21 BSD City",
        image: require("HomeBaseTest/app/assets/dummy/arsip-2.png"),
        komisi: "2%",
        title: "Foresta Ultimo BSD City",
        price: "Rp 2.500.000.000",
        type: "Rumah",
        label: 0, // 0: Dijual, 1: Disewa
        address: "Jl. Edutown Kav III.1, BSD, Tangerang Selatan",
        room: "3+1",
        bath: "2+1",
        square: "118m",
        isSold: true
    }
]