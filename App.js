import React, {Component, useState, useEffect} from 'react';
import {StyleSheet, View, ImageBackground, Dimensions, ScrollView, Button, TouchableOpacity} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"
import Header from "HomeBaseTest/app/components/header"
import Menu from "HomeBaseTest/app/components/menu"
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';

import Icon from 'react-native-vector-icons/Ionicons';

// Detail Listing
import ImageListing from "HomeBaseTest/app/components/detail/image"
import Description from "HomeBaseTest/app/components/detail/desc"
// import Info from "HomeBaseTest/app/components/detail/info"
import Notes from "HomeBaseTest/app/components/detail/notes"
import Owner from "HomeBaseTest/app/components/detail/owner"
import ButtonPromosi from "HomeBaseTest/app/components/detail/button"

import PushNotification from "react-native-push-notification";

const Stack = createStackNavigator();

const HomeScreen = (props) => {

    return (
      <View style={styles.container}>
        <Header />
        <Menu 
          {...props}
        />
      </View>
    );
}

const DetailScreen = (props) => {
  return(
    <ScrollView style={styles.container}>

        <ImageListing 
          {...props}
        />

        <Description {...props} />

        {/* <Info {...props} />*/}

        <Notes {...props} />

        <Owner {...props} />

        <ButtonPromosi />

    </ScrollView>
  )
}

const App = () => {
  async function requestUserPermission() {
    // Must be outside of any component LifeCycle (such as `componentDidMount`).
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        console.warn("TOKEN:", token);
      },

      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);

        // process the notification

        // (required) Called when a remote is received or opened, or local notification is opened
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

      // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      onAction: function (notification) {
        console.log("ACTION:", notification.action);
        console.log("NOTIFICATION:", notification);

        // process the action
      },

      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: function(err) {
        console.error(err.message, err);
      },

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: true,
    });
  }

  useEffect(()=>{
    requestUserPermission()
  })

  return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} options={{headerShown: false}}/>
        <Stack.Screen name="Detail" component={DetailScreen} options={{
          headerRight: () => (
            <View style={{marginRight: 10}}>
              <Icon name="heart-outline" size={25} color={Var.COLOR_BLACK} />
            </View>
          ),
        }}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Var.COLOR_WHITE
  },
  scene: {
    flex: 1,
  },
});

export default App;
