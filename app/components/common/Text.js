import React from 'react'
import { Text as RNText } from "react-native"
import * as Var from 'HomeBaseTest/app/config/variable'

const Text = (props) => {
    return (
        <RNText
            style={[props.style,{
                fontFamily: props.style.fontFamily ? props.style.fontFamily : Var.FONT_REGULAR,
                color: props.style.color ? props.style.color : Var.COLOR_PURE_BLACK
            }]}
            {...props}
        >
            {props.children}
        </RNText>
    );
};

Text.defaultProps = {
    style: {
        fontFamily: Var.FONT_REGULAR
    }
}

export default Text