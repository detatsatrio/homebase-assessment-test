 
import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"

import Icon from 'react-native-vector-icons/Ionicons';

const Desc = (props) => {

    const [data, setData] = useState(props.route.params.data)

    return (
      <View style={styles.container}>
          <View style={styles.titleContainerStyle}>
            <Text style={styles.titleStyle}>
                {data.title}
            </Text>
            {data.title_type == 1 && (
              <View style={styles.titleTypeStyle}>
                <Text style={styles.titleTypeTextStyle}>
                  PRIVATE
                </Text>
              </View>
            )}
          </View>
          <Text style={styles.priceStyle}>
              {data.price}
          </Text>
          <View style={{borderBottomWidth: 1, borderBottomColor: "#ececec", marginTop: 10, marginBottom: 10}} />
          <View style={styles.typeContainerStyle}>
            <Text style={styles.typeStyle}>
                {data.type} untuk Dijual
            </Text>
          </View>
          <Text style={styles.addressStyle}>
            <Icon name="ios-location-outline" size={10} color={Var.COLOR_BLACK} /> {data.address}
          </Text>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15
  },
  titleStyle: {
    fontSize: 14
  },
  titleTypeStyle: {
    backgroundColor: Var.COLOR_BLACK,
    padding: 5,
    borderRadius: 10
  },
  titleTypeTextStyle: {
    color: Var.COLOR_WHITE,
    fontSize: 8
  },
  priceStyle: {
    fontSize: 20,
    fontFamily: Var.FONT_BOLD,
    color: Var.COLOR_BLACK
  },
  typeStyle: {
    marginRight: 10,
    fontSize: 14,
    fontFamily: Var.FONT_DEMI
  },
  labelStyle: {
    color: Var.COLOR_WHITE,
    fontSize: 10,
    fontFamily: Var.FONT_BOLD
  },
  addressStyle: {
    fontSize: 10
  },
  titleContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  typeContainerStyle: {
    flexDirection: 'row'
  },
  labelSellStyle: {
    backgroundColor: Var.COLOR_BLUE,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 3,
    justifyContent: 'center',
    fontSize: 10
  },
  labelRentStyle: {
    backgroundColor: Var.COLOR_GREEN,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 3,
    fontSize: 10
  }
});

export default Desc;