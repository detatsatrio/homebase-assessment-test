 
import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import * as Var from "HomeBaseTest/app/config/variable"
import Text from "HomeBaseTest/app/components/common/Text"
import Icon from 'react-native-vector-icons/Ionicons';

const Image = (props) => {

    console.warn(props.route.params.data.image,"propsdata")

    return (
      <View style={styles.container}>
        <Text style={styles.addressStyle}>
                <Icon name="lock-closed" size={12} color={Var.COLOR_BLACK} /> Owner Properti
            </Text>
            <Text style={styles.captionStyle}>
                Hanya anda yang dapat melihat informasi ini
            </Text>
            <Text style={styles.alamatStyle}>
            {"\n"}Nama: Jois Aprillio {"\n"}
            No.HP: 081388809999 
            </Text>

            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                <TouchableOpacity style={styles.buttonStyle}>
                    <Text style={styles.buttonText}>
                        <Icon name="call" size={16} color={Var.COLOR_BLACK} /> Telepon
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttonStyle}>
                    <Text style={styles.buttonText}>
                        <Icon name="chatbubble-outline" size={16} color={Var.COLOR_BLACK} /> Whatsapp
                    </Text>
                </TouchableOpacity>
            </View>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 20,
    marginRight: 20
  },
  buttonStyle:{
    borderColor: "#ececec",
    borderWidth: 2,
    borderRadius: 10,
    paddingLeft: 40,
    paddingRight: 40,
    paddingTop: 5,
    paddingBottom: 5
  },    
  buttonText: {
    fontFamily: Var.FONT_MEDIUM,
    color: Var.COLOR_BLACK
    },
  addressStyle: {
    fontFamily: Var.FONT_BOLD,
    color: Var.COLOR_BLACK
  },
  captionStyle: {
    color: Var.COLOR_BROWN,
    fontSize: 10
  },
  alamatStyle: {
    color: Var.COLOR_BLACK
  },
  labelKomisiStyle: {
    backgroundColor: Var.COLOR_BROWN,
    padding: 10,
    borderRadius: 10,
    width: 45,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },
  textKomisiTitleStyle: {
    color: Var.COLOR_WHITE,
    fontSize: 8
  },
  textKomisiStyle: {
    color: Var.COLOR_WHITE,
    fontFamily: Var.FONT_BOLD,
    fontSize: 16
  },
  textArsipTitleStyle: {
    fontFamily: Var.FONT_BOLD,
    color: Var.COLOR_BROWN
  },
  labelArsipStyle: {
    borderStyle: 'dotted',
    borderColor: Var.COLOR_BROWN,
    borderRadius: 1,
    borderWidth: 1,
    transform: ([{ rotateZ: '-10deg' }]),
    backgroundColor: "#FFFFFFE6",
    padding: 10
  }
});

export default Image;